{ config, pkgs, ... }:

{
  environment = {
    systemPackages = with pkgs; [
      acpi
      acpitool
      aspell
      aspellDicts.en
      audacity
      autoconf
      automake
      bash
      bitcoin
      cacert
      caudec
      cargoSnapshot
      chicken
      clang
      cryptsetup
      curl
      dmenu
      duplicity
      egg2nix
      emacs
      ffmpeg
      file
      filegive
      flac
      fuse
      gcc
      geeqie
      gimp
      gitAndTools.gitFull
      gnumake
      gnupg
      gnupg1compat
      graphviz
      haskellngPackages.xmobar
      htop
      iftop
      iputils
      isync
      jack2
      jdk
      lame
      leiningen
      libtool
      lsof
      manpages
      mercurial
      mixxx
      mosh
      mpc_cli
      mpd
      mpv
      msmtp
      ncdu
      ncmpc
      netcat-openbsd
      nix-repl
      nmap
      nodejs
      nodePackages.peerflix
      notmuch
      openssl
      openssh
      openvpn
      pamixer
      pandoc
      pass
      pavucontrol
      pdftk
      pinentry
      pkgconfig
      powertop
      pythonPackages.gdrivefs
      qjackctl
      ranger
      readline
      redshift
      ruby_2_2
      rxvt_unicode
      scrot
      silver-searcher
      smartmontools
      sshfsFuse
      sqlite
      stow
      subversion
      supercollider_scel
      syncthing
      tmux
      torbrowser
      traceroute
      tree
      unclutter
      unrar
      unzip
      valgrind
      wget
      which
      wirelesstools
      wmctrl
      wpa_supplicant
      wpa_supplicant_gui
      x11
      xbindkeys
      xdotool
      xclip
      xlibs.xdpyinfo
      xlibs.xmessage
      xlibs.xmodmap
      xlibs.xset
      xsel
      xscreensaver
      youtube-dl
      zathura
      zile
      zip
      zsh
    ];
  };

  nixpkgs = {
    config = {
      allowUnfree = true;
      dmenu = {
	enableXft = true;
      };
      firefox = {
	enableAdobeFlash = true;
      };
      mpd = {
	pulseaudioSupport = true;
      };
    };
  };
}
