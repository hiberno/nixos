{ config, pkgs, ... }:

{
  imports = [
    ./packages.nix
  ];

  boot = {
    extraModprobeConfig = ''
      options thinkpad_acpi fan_control=1
      options i915 i915_enable_rc6=1 i915_enable_fbc=1 lvds_downclock=1
      options snd slots=snd-hda-intel
    '';
    extraModulePackages = [
      config.boot.kernelPackages.tp_smapi
    ];
    initrd = {
      kernelModules = [
        "ahci"
        "dm_mod"
        "dm-crypt"
        "ehci_pci"
        "fbcon"
        "i915"
	"snd-seq"
	"snd-rawmidi"
        "xhci_hcd"
      ];
      luks = {
        devices = [{
          name = "nixos-root";
          device = "/dev/sdb2";
          allowDiscards = true;
        }];
      };
   };
   loader = {
     grub = {
       device = "/dev/sdb";
       memtest86 = {
         enable = true;
       };
       version = 2;
     };
   };
   kernelModules = [
     "fuse"
     "kvm_intel"
     "msr"
     "tp-smapi"
     "tun"
   ];
   kernelPackages = pkgs.linuxPackages_latest;
   kernelParams = [
     "zfs.zfs_arc_max=1073741824"
   ];
   supportedFilesystems = [ "zfs" ];
   vesa = false;
   zfs = {
     useGit = true;
     forceImportRoot = false;
     forceImportAll = false;
   };
  };

  environment = {
    shells = [
      "${pkgs.bash}/bin/bash"
      "${pkgs.zsh}/bin/zsh"
    ];
    variables = {
      BROWSER = pkgs.lib.mkOverride 0 "chromium";
      EDITOR = pkgs.lib.mkOverride 0 "zile";
    };
  };

  fileSystems = {
    "/" = {
      device = "zroot/ROOT/nixos";
      fsType = "zfs";
    };
    "/home/" = {
      device = "zroot/HOME/_1126";
      fsType = "zfs";
    };
    "/boot" = {
      device = "/dev/sdb1";
      fsType = "ext2";
    };
  };

  fonts = {
    fontconfig = {
      defaultFonts = {
        monospace = [
	  "Source Code Pro"
	  "Terminus"
	];
	sansSerif = [
	  "DejaVu Sans"
	];
        serif = [
	  "DejaVu Serif"
	];
      };
      enable = true;
    };
    fonts = with pkgs; [
      corefonts
      source-code-pro
      terminus_font
    ];
  };

  hardware = {
    cpu = {
      intel = {
        updateMicrocode = true;
      };
    };
    enableAllFirmware = true;
    opengl = {
      driSupport32Bit = true;
    };
    pulseaudio = {
      enable = true;
      package = pkgs.pulseaudio.override { jackaudioSupport = true; };
      };
  };

  i18n = {
    consoleFont = "lat9w-16";
    consoleKeyMap = "de";
    defaultLocale = "en_GB.UTF-8";
  };

  networking = {
    enableIPv6 = true;
    firewall = {
      enable = true;
      allowPing = false;
    };
    hostId = "23a1a742";
    hostName = "piet";
    wireless = {
      enable = true;
      interfaces = [ "wlp3s0" ];
      userControlled = {
        enable = true;
        group = "network";
      };
    };
  };

  nix = {
    extraOptions = ''
      gc-keep-outputs = true
      gc-keep-derivations = true
    '';
    maxJobs = 4;
    package = pkgs.nixUnstable;
    binaryCaches = [ https://cache.nixos.org https://hydra.nixos.org http://hydra.cryp.to ];
    trustedBinaryCaches = config.nix.binaryCaches;
  };

  powerManagement = {
    cpuFreqGovernor = "ondemand";
    enable = true;
    scsiLinkPolicy = "min_power";
  };

  programs = {
    ssh = {
      startAgent = false;
    };
  };

  services = {
    acpid = {
      enable = true;
    };
    nixosManual = {
      showManual = true;
    };
    ntp = {
      enable = true;
    };
    peerflix = {
      enable = true;
    };
    printing = {
      drivers = with pkgs; [
        hplip
      ];
      enable = true;
    };
    redshift = {
      enable = true;
      latitude = "50.95";
      longitude = "6.67";
      temperature = {
        day = 5800;
        night = 4500;
      };
    };
    xserver = {
      autorun = true;
      displayManager = {
        sessionCommands = ''
	  ${pkgs.rxvt_unicode}/bin/urxvtd -q -o -f &
          ${pkgs.unclutter}/bin/unclutter -idle 1 &
          ${pkgs.xlibs.xf86inputsynaptics}/bin/syndaemon -t -k -i 3 -d &
          ${pkgs.xscreensaver}/bin/xscreensaver -no-splash &
          ${pkgs.haskellngPackages.xmobar}/bin/xmobar & disown
        '';
        slim = {
          autoLogin = true;
          defaultUser = "hiberno";
          enable = true;
        };
      };
      desktopManager = {
        xterm = {
          enable = false;
        };
        default = "none";
      };
      enable = true;
      layout = "de";
      startGnuPGAgent = true;
      synaptics = {
        enable = true;
        twoFingerScroll = true;
      };
      vaapiDrivers = [
        pkgs.vaapiIntel
      ];
      videoDrivers = [
        "intel"
      ];
      windowManager = {
        default = "herbstluftwm";
	herbstluftwm = {
          enable = true;
        };
      };
      xkbModel = "thinkpad60";
      xkbOptions = "ctrl:nocaps";
    };
    zfs = {
      autoSnapshot = {
        daily = 7;
        enable = true;
        frequent = 4;
        hourly = 6;
        weekly = 3;
      };
    };
  };

  security = {
    sudo = {
      enable = true;
    };
  };

  systemd = {
    services = {
      lockscreen = {
        description = "Lock screen when suspending.";
        environment = { DISPLAY = ":0"; };
        serviceConfig = {
          User = "hiberno";
          Type = "forking";
          ExecStart = "${pkgs.xscreensaver}/bin/xscreensaver-command --lock ";
        };
        wantedBy = [ "sleep.target" ];
      };
    };
  };

  time = {
        timeZone = "Europe/Berlin";
  };

  users = {
    extraGroups = {
      network = {
        gid = 700;
      };
      systemd-journal = { };
    };
    extraUsers = {
      hiberno = {
        createHome = false;
        description = "Christian Lask";
        extraGroups = [
          "audio"
          "messagebus"
          "network"
          "systemd-journal"
	  "video"
          "wheel"
        ];
        group = "users";
        home = "/home/hiberno";
        password = "foo";
        shell = "${pkgs.zsh}/bin/zsh";
        uid = 1000;
      };
    };
  };
}
