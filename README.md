nixos-configuration
===================

This is how my [NixOS](https://nixos.org/) installation looks like.

Some remarks:
* Full disk encryption (except for /boot, obviously).
* ZFS as filesystem.
* Runs on a Thinkpad X220.
